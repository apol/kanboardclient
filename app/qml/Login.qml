import QtQuick 2.2
import QtQuick.Controls 1.3
import QtQuick.Layouts 1.1

Rectangle {
    id: root
    color: "white"
    property alias serverUrl: serverTextField.text
    property alias apiKey: apiKeyTextField.text

    signal login(string serverPath, string apiKey);

    ColumnLayout {
        id: loginForm
        anchors.centerIn: parent
        width: parent.width / 2

        TextField {
            id: serverTextField
            Layout.fillWidth: true
            placeholderText: "Server"
            inputMethodHints: Qt.ImhNoAutoUppercase | Qt.ImhNoPredictiveText
        }
        TextField {
            id: apiKeyTextField
            Layout.fillWidth: true
            placeholderText: "API Key"
            inputMethodHints: Qt.ImhNoAutoUppercase | Qt.ImhNoPredictiveText | Qt.ImhSensitiveData
        }
        Button {
            text: "Login"
            Layout.alignment: Qt.AlignHCenter
            onClicked: root.login(serverTextField.text, apiKeyTextField.text);
        }
        z: 10
    }

    Rectangle {
        anchors.fill: loginForm
        anchors.margins: -16
        radius: 4
        color: "#BBBBEE"
        width: 10
        height: 10
    }
}
