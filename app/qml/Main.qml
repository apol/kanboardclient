import QtQuick 2.0
import QtQuick.Controls 1.3
import QtQuick.Layouts 1.1
import Qt.labs.settings 1.0

import Kanboard 1.0

import "components"

ApplicationWindow
{
    width: 800
    height: 600
    visible: true

    toolBar: ToolBar {
        RowLayout {
            Button {
                text: "Logout"
                enabled: KanboardInterface.isAuthenticated
                onClicked: {
                    KanboardInterface.url = ""
                }
            }
            ComboBox {
                enabled: KanboardInterface.isAuthenticated
                model: BoardsModel {}
                textRole: "display"
                onCurrentTextChanged: {
                    tasksModel.projectId = model.get(currentIndex).id
                }
            }
            BusyIndicator {
                running: tasksModel.isFetching
            }
        }
    }

    Connections {
        target: KanboardInterface
        onUrlChanged: authSettings.url = KanboardInterface.url

    }
    Settings {
        id: authSettings

        category: "Authentication"
        property string url
    }

    Item {
        anchors.fill: parent

        Login {
            anchors.fill: parent
            visible: !KanboardInterface.isAuthenticated

            onLogin: {
                KanboardInterface.url = serverPath.replace("://", "://jsonrpc:"+apiKey+"@");
            }
            Component.onCompleted: {
                var match = authSettings.url.match(/(https?:\/\/)jsonrpc:([a-z0-9]+)@(.*)/i);

                if (match) {
                    serverUrl = match[1]+match[3]
                    apiKey = match[2]
                }
            }
        }
        SplitView {
            anchors.fill: parent
            ScrollView {
                id: main
                visible: KanboardInterface.isAuthenticated
                Layout.fillWidth: true
                Board {
                    id: board
                    width: Math.max(main.viewport.width, implicitWidth)
                    height: implicitHeight

                    tasksModel: TasksModel {
                        id: tasksModel
                    }
                }
            }
            TaskView {
                id: taskView
                task: board.activeTask
                visible: task != undefined
            }
        }
    }
}
