import QtQuick 2.2
import QtQuick.Layouts 1.1
import org.kde.kquickcontrolsaddons 2.0 as KQuick


Item {
    id: board
    width: 800 //these are just for testing if board.qml is loaded independently
    height: 600

    implicitWidth: columns.Layout.minimumWidth + 8
    implicitHeight: columns.implicitHeight + 8

    property QtObject tasksModel: columnsTestData

    property var activeTask: undefined

    RowLayout {
        id: columns
        anchors.fill: parent
        anchors.margins: 4
        spacing: 4

        Repeater {
            model: tasksModel
            delegate: TaskColumn {
                Layout.minimumWidth: 250
                Layout.maximumWidth: 400
                Layout.fillHeight: true
                Layout.fillWidth: true

                tasksListModel: KQuick.ColumnProxyModel {
                    rootIndex: tasksModel.index(index, 0)
                }

                onTaskSelected: {
                    activeTask = task
                }
            }
        }
    }
}