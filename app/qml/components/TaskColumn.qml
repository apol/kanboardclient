import QtQuick 2.2
import QtQuick.Layouts 1.1
import QtQuick.Controls 1.2

Rectangle {
    id: root
    border.color: "#eeeeee"
    border.width: 1
    implicitHeight: header.implicitHeight + main.implicitHeight + 12

    property int columnId: info.id
    property alias tasksListModel: repeater.model
    signal taskSelected(var task)

    RowLayout {
        id: header
        anchors.top: parent.top
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.margins: 4
        Label {
            text: "+"
            font.bold: true
            font.pixelSize: 16
//                 onClicked: console.log("NEW ITEM CLICKED") //FIXME
        }

        Label {
            text: "<b>"+display+"</b> (" + repeater.count + ")"
        }
    }

    ColumnLayout {
        id: main
        
        anchors.top: header.bottom
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.margins: 4        
        
        opacity: dropArea.containsDrag ? 0.3 : 1.0

        Repeater {
            id: repeater
            model: taskListTestData.qml
            delegate: Item {
                id: dragRoot
                Layout.fillWidth: true
                implicitHeight: tile.implicitHeight

                TaskDelegate {
                    id: tile
                    z: Drag.active ? 1000 : 0
                    property int taskId: info.id

                    //this is NOT anchors.fill so we can break the anchors in D&D
                    anchors.left: parent.left
                    anchors.right: parent.right
                    anchors.bottom: parent.bottom
                    anchors.top: parent.top

                    Drag.active: mouseArea.drag.active
                }

                MouseArea {
                    id: mouseArea
                    anchors.fill: parent
                    drag.target: tile
                    
                    onReleased: tile.Drag.drop()
                    
                    onDoubleClicked: {
                        console.log(info)
                        taskSelected(info);
                    }
                }

                states: State {
                    when: mouseArea.drag.active
                    ParentChange { target: tile; parent: root.parent.parent}//z-index hack
                    PropertyChanges { target: tile; z: 1000}

                    AnchorChanges { target: tile; anchors.top: undefined; anchors.bottom: undefined; anchors.left: undefined; anchors.right: undefined}
                }
            }
        }
        Rectangle {
            visible: dropArea.containsDrag
            Layout.fillWidth: true
            radius: 4
            height: 50

            border.width: 1
            Text {
                anchors.centerIn: parent
                text: "Drop"
            }
        }
    }
    DropArea {
        id: dropArea
        anchors.fill: parent
        onDropped: {
            console.log("TASK ", drag.source.taskId, "dropped into column " , root.columnId)
            
            //TODO referencing variable not in local scope ==> bad QML
            board.tasksModel.moveTaskPosition(drag.source.taskId, root.columnId, 0);
        }
    }
    
}
