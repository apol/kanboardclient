import QtQuick 2.2
import QtQuick.Layouts 1.1
import QtQuick.Controls 1.2

Rectangle {
    color: Qt.lighter(info.color_id || "yellow", 1.9)
    implicitHeight: layout.implicitHeight + 6

    ColumnLayout {
        id: layout
        anchors.fill: parent
        anchors.margins: 3
        Label {
            id: assigneeLabel
            font.bold: true
            text: info.assignee_name ? "Assigned to " + info.assignee_name : "Nobody assigned"
            elide: Text.ElideRight
            Layout.fillWidth: true
        }

        Label {
            text: (display + "\n Comments:"+info.nb_comments+"\n Subtasks:"+info.nb_completed_subtasks+"/"+info.nb_subtasks)
            wrapMode: Text.Wrap
            Layout.fillWidth: true
            Layout.fillHeight: true

        }
    }

    border.color: info.color_id || "black"
    border.width: 1
}
