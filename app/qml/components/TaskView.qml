import QtQuick 2.3
import QtQuick.Controls 1.2
import QtQuick.Layouts 1.1

import Kanboard 1.0

// import org.kde.plasma.extras 2.0 as PlasmaExtras

ColumnLayout {
    width: 300
    height: 500
    property var task: undefined
    property alias editMode: editButton.checked

    onTaskChanged: {
        console.log("Task set to :",  JSON.stringify(task))
    }

    function save() {
        //FUTURE if(task) {update} else {create}
        console.log("DAVE - udpating ", task.id);
        KanboardInterface.updateTask(task.id,
                   titleField.text, task.project_id, task.color_id,
                   task.column_id, descriptionField.text, task.owner_id,
                   task.creator_id, task.score, task.dueDate, task.category_id);
        editMode = false;
    }

    //I'll redo this
    Button {
        id: editButton
        checkable: true
        iconSource: "document-edit-verify"
        text: "Edit"
    }
    
    Label {//PlasmaExtras.Title {
        text: task.title || ""
        font.bold: true
        visible: !editMode
    }
    
    TextField {
        id: titleField
        Layout.fillWidth: true
        placeholderText: "Title"
        text: task.title || ""
        visible: editMode
    }

    Label {//PlasmaExtras.Heading {
//         level: 3
        text: "Description"
        font.bold: true
    }
    Label {
        Layout.fillWidth: true
        text: task.description || ""
        wrapMode: Text.Wrap
        textFormat: Text.PlainText
        visible: !editMode
    }
    
    TextArea {
        id: descriptionField
        Layout.fillWidth: true
        text: task.description
        wrapMode: Text.Wrap
        visible: editMode
    }
    
    Button {
        //I'll remove this later with some sort of auto save on close
        text: "Save"
        onClicked: save();
        visible: editMode
    }
    
    
    Label {//PlasmaExtras.Heading {
//         level: 3
        text: "Sub-Tasks"
        font.bold: true
    }
    TableView {
        Layout.fillWidth: true
        model: SubtasksModel {
            taskId: task.id || -1
        }
        TableViewColumn{ role: "title"  ; title: "Title" ; width: 250 }
        TableViewColumn{ role: "status" ; title: "Status" ; width: 50 }
        TableViewColumn{ role: "assignee" ; title: "Assignee" ; width: 200 }

        itemDelegate: textDelegate

        Component {
            id: textDelegate
            Text {
                anchors.verticalCenter: parent.verticalCenter
                color: styleData.textColor
                elide: styleData.elideMode
                text: {
                    if (styleData.role != "status") {
                        return styleData.value
                    } else {
                        if (styleData.value == SubtasksModel.StateDone)
                            return "Done"
                        else if (styleData.value == SubtasksModel.StateInProgress)
                            return "In Progress"
                        else
                            return "TODO"
                    }
                }
            }
        }
    }


    Label {//PlasmaExtras.Heading {
//         level: 3
        text: "Comments"
        font.bold: true
    }

    //REPLACE ME
    TableView {
        Layout.fillWidth: true
        model: task.comments
        TableViewColumn{ role: "author"  ; title: "Author" ; width: 250 }
        TableViewColumn{ role: "text"  ; title: "Text" ; width: -1}
    }
}
