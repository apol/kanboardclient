import QtQuick 2.0

ListModel {
    ListElement {
        title: "Foo"
        description: "I am a task"
        assignee: "Aleix Pol"
    }
    ListElement {
        title: "Bar"
        description: "New task"
        assignee: "David Edmundson"
    }
    ListElement {
        title: "This is a really long description of a task which is pointlessly wordy"
        assignee: ""
    }
}