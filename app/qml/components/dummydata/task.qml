import QtQuick 2.0

QtObject
{
    property string createdBy: "David Edmundson"
    property string title: "Keyboard Layout Selector"
    property string assignedTo: "Martin Yrj�l�"
    property string description: "This used to be part of keyboard switcher kcm in kde-runtime. \nI want to add an applet, make it dbus activated then kill the systray icon in the current daemon."

    //category
    //documents

    //time tracking (later)

    property QtObject subTasks: ListModel {
        ListElement {
            subtaskId: "1"
            title: "dbus plugin for qml from the screen locker"
            status: "Done"
            assignee: ""
            //timetracking
        }
        ListElement {
            subtaskId: "2"
            title: "Make plasmoid autoload      "
            status: "Todo"
            assignee: ""
        }
        ListElement {
            subtaskId: "3"
            title: "Remove SNI from keyboard layout daemon"
            status: "Done"
            assignee: ""
        }
    }
    property QtObject comments: ListModel {
        ListElement {
            commentId: "1"
            author: "Foobar"
            date: "3/9/2014"
            text: "This could be interesting. I'm not aware of dbus support in QML, so if I'm right, this plasmoid will need a C++ plugin? Do you happen to know a good example of a QML plasmoid with a C++ plugin, which uses DBus?"
        }
        ListElement {
            commentId: "2"
            author: "David Edmundson"
            date: "3/9/2014"
            text: ""
        }

    }

//     function addSubtask(//all the params)
//     function removeSubtask(subtaskId)
//     function addComment(text) {console.log("Adding comment " + text)}
//     function removeComment(commentId)
}