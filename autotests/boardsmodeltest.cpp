/*
 *   Copyright 2014 Aleix Pol Gonzalez <aleixpol@kde.org>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU Library General Public License as
 *   published by the Free Software Foundation; either version 2, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details
 *
 *   You should have received a copy of the GNU Library General Public
 *   License along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#include "boardsmodeltest.h"
#include <declarative/kanboardinterface.h>
#include <declarative/boardsmodel.h>
#include "config.h"

#include <QtTest>

QTEST_MAIN(BoardsModelTest);

BoardsModelTest::BoardsModelTest(QObject* parent): QObject(parent)
{
}

void BoardsModelTest::listBoardsTest()
{
    KanboardInterface::self()->setUrl(QUrl::fromLocalFile(SRCDIR "/listBoards"));
    BoardsModel m;
    if (m.isFetching()) {
        QSignalSpy spy(&m, SIGNAL(fetchingChanged()));
        QVERIFY(spy.wait());
    }
    QCOMPARE(m.rowCount(), 1);
}

#include "boardsmodeltest.moc"
