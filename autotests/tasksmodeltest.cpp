/*
 *   Copyright 2014 Aleix Pol Gonzalez <aleixpol@kde.org>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU Library General Public License as
 *   published by the Free Software Foundation; either version 2, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details
 *
 *   You should have received a copy of the GNU Library General Public
 *   License along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#include "tasksmodeltest.h"
#include <declarative/kanboardinterface.h>
#include <declarative/tasksmodel.h>

#include "config.h"

#include <QtTest>

QTEST_MAIN(TasksModelTest);

void TasksModelTest::testFetchBoard()
{
    KanboardInterface::self()->setUrl(QUrl::fromLocalFile(SRCDIR "/fetchBoard"));
    TasksModel m;
    m.setProjectId(1);
    QSignalSpy spy(&m, SIGNAL(fetchingChanged()));
    QVERIFY(spy.wait());
    QCOMPARE(m.rowCount(), 4);

    QModelIndex idx = m.index(0, 0);
    QCOMPARE(idx.data().toString(), QStringLiteral("Backlog"));
    QCOMPARE(idx.child(0,0).data().toString(), QStringLiteral("Hello"));
}

#include "tasksmodeltest.moc"
