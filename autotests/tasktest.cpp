/*
 *   Copyright 2014 Aleix Pol Gonzalez <aleixpol@kde.org>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU Library General Public License as
 *   published by the Free Software Foundation; either version 2, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details
 *
 *   You should have received a copy of the GNU Library General Public
 *   License along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#include "tasktest.h"
#include "config.h"
#include <declarative/task.h>
#include <declarative/kanboardinterface.h>

#include <QtTest>

QTEST_MAIN(TaskTest);

void TaskTest::fetchTest()
{
    KanboardInterface::self()->setUrl(QUrl::fromLocalFile(SRCDIR "/fetchTest"));
    Task t;
    t.setTaskId(1);
    QSignalSpy spy(&t, SIGNAL(taskChanged()));
    QVERIFY(spy.wait());
    QCOMPARE(t.title(), QStringLiteral("Hello"));
}

void TaskTest::changeTaskTest()
{
    KanboardInterface::self()->setUrl(QUrl::fromLocalFile(SRCDIR "/changeTaskTest"));
    Task t;
    t.setTaskId(1);
    QSignalSpy spy(&t, SIGNAL(taskChanged()));
    QVERIFY(spy.wait());
    QCOMPARE(t.title(), QStringLiteral("Hello"));
    t.setTitle("Hola");
    QVERIFY(spy.wait());
    QCOMPARE(t.title(), QStringLiteral("Hola"));
    t.setTitle("Hello");
    QVERIFY(spy.wait());
    QCOMPARE(t.title(), QStringLiteral("Hello"));
}

#include "tasktest.moc"
