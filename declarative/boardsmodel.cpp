/*
 *   Copyright 2014 Aleix Pol Gonzalez <aleixpol@kde.org>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU Library General Public License as
 *   published by the Free Software Foundation; either version 2, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details
 *
 *   You should have received a copy of the GNU Library General Public
 *   License along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#include "boardsmodel.h"
#include "kanboardinterface.h"
#include <QtNetwork/QNetworkReply>
#include <QtCore/QJsonDocument>

BoardsModel::BoardsModel(QObject* parent)
    : QAbstractListModel(parent)
{
    fetch();
    connect(KanboardInterface::self(), SIGNAL(urlChanged(QUrl)), SLOT(fetch()));
}

void BoardsModel::fetch()
{
    if (!KanboardInterface::self()->isAuthenticated())
        return;
    m_reply = KanboardInterface::self()->getAllProjects();
    connect(m_reply, SIGNAL(finished()), SLOT(projectsFetched()));
    emit fetchingChanged();
}

QVariant BoardsModel::data(const QModelIndex& index, int role) const
{
    if (!index.isValid() || index.row()>=m_result.count())
        return QVariant();
//     TODO keep them as QList<QVariantMap>?
    switch(role) {
        case Qt::DisplayRole:
            Q_ASSERT(m_result[index.row()].isObject());
            return m_result[index.row()].toObject().value("name");
        case Qt::UserRole:
            return m_result[index.row()];
    }
    return QVariant();
}

int BoardsModel::rowCount(const QModelIndex& parent) const
{
    return parent.isValid() ? 0 : m_result.count();
}

void BoardsModel::projectsFetched()
{
    QJsonValue resultObject = KanboardInterface::fetchResult(m_reply);
    Q_ASSERT(!resultObject.isUndefined() || resultObject.isArray());

    beginResetModel();
    m_result = resultObject.toArray();
    endResetModel();

    emit fetchingChanged();
}

bool BoardsModel::isFetching() const
{
    return m_reply;
}

QHash<int, QByteArray> BoardsModel::roleNames() const
{
    QHash<int, QByteArray> ret = QAbstractItemModel::roleNames();
    ret.insert(Qt::UserRole, "info");
    return ret;
}

QJsonObject BoardsModel::get(int row) const
{
    return m_result[row].toObject();
}
