/*
 *   Copyright 2014 Aleix Pol Gonzalez <aleixpol@kde.org>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU Library General Public License as
 *   published by the Free Software Foundation; either version 2, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details
 *
 *   You should have received a copy of the GNU Library General Public
 *   License along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#ifndef BOARDSMODEL_H
#define BOARDSMODEL_H

#include <QtCore/QAbstractListModel>
#include <QtCore/QPointer>
#include <QtCore/QJsonArray>
#include <QtCore/QJsonObject>

class QNetworkReply;
class BoardsModel : public QAbstractListModel
{
    Q_OBJECT
    Q_PROPERTY(bool isFetching READ isFetching NOTIFY fetchingChanged)
    public:
        explicit BoardsModel(QObject* parent = 0);

        virtual QVariant data(const QModelIndex& index, int role = Qt::DisplayRole) const;
        virtual int rowCount(const QModelIndex& parent = QModelIndex()) const;
        virtual QHash<int, QByteArray> roleNames() const;

        bool isFetching() const;
        Q_SCRIPTABLE QJsonObject get(int row) const;

    private slots:
        void projectsFetched();
        void fetch();

    signals:
        void fetchingChanged();

    private:
        QJsonArray m_result;
        QPointer<QNetworkReply> m_reply;
};

#endif // BOARDSMODEL_H
