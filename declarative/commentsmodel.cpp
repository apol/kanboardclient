/*
 *   Copyright 2014 Aleix Pol Gonzalez <aleixpol@kde.org>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU Library General Public License as
 *   published by the Free Software Foundation; either version 2, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details
 *
 *   You should have received a copy of the GNU Library General Public
 *   License along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#include "commentsmodel.h"
#include "kanboardinterface.h"
#include <QtNetwork/QNetworkReply>
#include <QtCore/QJsonValue>
#include <QtCore/QJsonObject>
#include <QtCore/QDebug>

CommentsModel::CommentsModel(QObject* parent)
    : QAbstractListModel(parent)
    , m_taskId(-1)
{
    connect(KanboardInterface::self(), SIGNAL(authenticatedChanged(bool)), SLOT(fetch()));
}

QHash< int, QByteArray > CommentsModel::roleNames() const
{
    QHash<int, QByteArray> roles = QAbstractItemModel::roleNames();
    roles.insert(Qt::UserRole, "info");
    return roles;
}

void CommentsModel::setTaskId(int taskId)
{
    m_taskId = taskId;
    fetch();
}

int CommentsModel::taskId() const
{
    return m_taskId;
}

void CommentsModel::fetch()
{
    if (!KanboardInterface::self()->isAuthenticated()) {
        qWarning() << "cannot fetch if not authenticated";
        return;
    }

    if (m_taskId < 0)
        return;

    Q_ASSERT(m_taskId>=0);
    QNetworkReply* reply = KanboardInterface::self()->getAllComments(m_taskId);
    connect(reply, SIGNAL(finished()), SLOT(receivedAllComments()));
}

void CommentsModel::receivedAllComments()
{
    QNetworkReply* reply = qobject_cast<QNetworkReply*>(sender());
    QJsonValue resultObject = KanboardInterface::fetchResult(reply);
    if (resultObject.isUndefined() || !resultObject.isArray()) {
        qCritical() << "wrong result" << resultObject;
        return;
    }

    beginResetModel();
    m_results = resultObject.toArray();
    endResetModel();
}

QVariant CommentsModel::data(const QModelIndex& index, int role) const
{
    if (!index.isValid() || index.row()>=m_results.count())
        return QVariant();

    switch (role) {
        case Qt::DisplayRole:
            return m_results[index.row()].toObject().value("comment").toString();
        case Qt::UserRole:
            return m_results[index.row()].toObject();
    }
    return QVariant();
}

int CommentsModel::rowCount(const QModelIndex& parent) const
{
    return parent.isValid() ? 0 : m_results.count();
}
