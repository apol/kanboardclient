/*
 *   Copyright 2014 Aleix Pol Gonzalez <aleixpol@kde.org>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU Library General Public License as
 *   published by the Free Software Foundation; either version 2, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details
 *
 *   You should have received a copy of the GNU Library General Public
 *   License along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#include "kanboardinterface.h"
#include <QtCore/QFile>
#include <QtCore/QJsonObject>
#include <QtCore/QJsonDocument>
#include <QtCore/QDebug>
#include <QtCore/QDateTime>
#include <QtNetwork/QNetworkAccessManager>
#include <QtNetwork/QNetworkRequest>
#include <QtNetwork/QNetworkAccessManager>
#include <QtNetwork/QNetworkReply>
#ifdef QT_NO_SSL
#error Your Qt does not have ssl
#endif

Q_GLOBAL_STATIC(KanboardInterface, s_self)

KanboardInterface::KanboardInterface()
    : m_manager(new QNetworkAccessManager(this))
    , m_idCount(0)
{
}

KanboardInterface* KanboardInterface::self()
{
    return s_self;
}

QNetworkReply* KanboardInterface::createRequest(const QString& method, const QJsonObject& args)
{
    Q_ASSERT(isAuthenticated());

    QJsonObject requestObject;
    requestObject.insert("jsonrpc", "2.0");
    requestObject.insert("id", ++m_idCount);
    requestObject.insert("method", method);
    if (!args.isEmpty())
        requestObject.insert("params", args);

    QNetworkReply* ret = nullptr;
    if (Q_LIKELY(!m_url.isLocalFile())) { //normal mode
        QByteArray head = "Basic " + m_url.userInfo().toLatin1().toBase64();

        QNetworkRequest req(m_url);
        req.setRawHeader("Authorization", head);
        req.setHeader(QNetworkRequest::ContentTypeHeader, QStringLiteral("application/x-www-form-urlencoded"));
        QByteArray reqData = QJsonDocument(requestObject).toJson(QJsonDocument::Compact);
        ret = m_manager->post(req, reqData);
    } else { //test mode
        QString pathReq = m_url.toLocalFile()+'/'+QString::number(m_idCount)+'_'+method+"_request.json";
        QFile fileReq(pathReq);
        bool b = fileReq.open(QIODevice::ReadOnly);
        if (!b) {
            qCritical() << "couldn't open file:\n" << qPrintable(pathReq) << "\nexpected:\n" << requestObject;
            Q_UNREACHABLE();
        }
        QJsonObject testObject = QJsonDocument::fromJson(fileReq.readAll()).object();
        if (testObject != requestObject) {
            qCritical() << "error request\ntest:" << testObject << "\n req:"<< requestObject;
            Q_UNREACHABLE();
        }

        QUrl fileReply = QUrl::fromLocalFile(m_url.toLocalFile()+'/'+QString::number(m_idCount)+'_'+method+"_reply.json");
        ret = m_manager->get(QNetworkRequest(fileReply));
    }
    connect(ret, SIGNAL(finished()), ret, SLOT(deleteLater()));
    return ret;
}

void KanboardInterface::setUrl(const QUrl& url)
{
    m_idCount = 0;

    if (m_url != url) {
        bool wasAuthenticated = isAuthenticated();

        m_url = url;
        emit urlChanged(url);

        bool nowAuthenticated = isAuthenticated();
        if (wasAuthenticated != nowAuthenticated)
            emit authenticatedChanged(nowAuthenticated);
    }
}

QUrl KanboardInterface::url() const
{
    return m_url;
}

bool KanboardInterface::isAuthenticated() const
{
    return !m_url.password().isEmpty() || m_url.isLocalFile();
}

QJsonValue KanboardInterface::fetchResult(QNetworkReply* reply)
{
    if (reply->error() != QNetworkReply::NoError) {
        qWarning() << "error when fetching the tasks" << reply->errorString();
        return QJsonValue();
    }

    QByteArray data = reply->readAll();
    QJsonParseError error;
    QJsonDocument doc = QJsonDocument::fromJson(data, &error);

    if (error.error) {
        qCritical() << "json error" << error.errorString();
        return QJsonValue();
    }
    return doc.object().value("result");
}

//From now on we're just implementing the API, so it should just be boilerplate code.

QNetworkReply* KanboardInterface::createProject(const QString& name)
{
    QJsonObject obj;
    obj.insert("name", name);
    return createRequest("createProject", obj);
}

QNetworkReply* KanboardInterface::getProjectById(int project_id)
{
    QJsonObject obj;
    obj.insert("project_id", project_id);
    return createRequest("getProjectById", obj);
}

QNetworkReply* KanboardInterface::getProjectByName(const QString& name)
{
    QJsonObject obj;
    obj.insert("name", name);
    return createRequest("getProjectByName", obj);
}

QNetworkReply* KanboardInterface::getAllProjects()
{
    QJsonObject obj;
    return createRequest("getAllProjects", obj);
}

QNetworkReply* KanboardInterface::updateProject(int project_id, const QString& name, bool is_active, const QString& token, bool is_public)
{
    QJsonObject obj;
    obj.insert("project_id", project_id);
    obj.insert("name", name);
    obj.insert("is_active", is_active);
    obj.insert("token", token);
    obj.insert("is_public", is_public);
    return createRequest("updateProject", obj);
}

QNetworkReply* KanboardInterface::removeProject(int project_id)
{
    QJsonObject obj;
    obj.insert("project_id", project_id);
    return createRequest("removeProject", obj);
}

QNetworkReply* KanboardInterface::enableProject(int project_id)
{
    QJsonObject obj;
    obj.insert("project_id", project_id);
    return createRequest("enableProject", obj);
}

QNetworkReply* KanboardInterface::disableProject(int project_id)
{
    QJsonObject obj;
    obj.insert("project_id", project_id);
    return createRequest("disableProject", obj);
}

QNetworkReply* KanboardInterface::enableProjectPublicAccess(int project_id)
{
    QJsonObject obj;
    obj.insert("project_id", project_id);
    return createRequest("enableProjectPublicAccess", obj);
}

QNetworkReply* KanboardInterface::disableProjectPublicAccess(int project_id)
{
    QJsonObject obj;
    obj.insert("project_id", project_id);
    return createRequest("disableProjectPublicAccess", obj);
}

QNetworkReply* KanboardInterface::getBoard(int project_id)
{
    QJsonObject obj;
    obj.insert("project_id", project_id);
    return createRequest("getBoard", obj);
}

QNetworkReply* KanboardInterface::getColumns(int project_id)
{
    QJsonObject obj;
    obj.insert("project_id", project_id);
    return createRequest("getColumns", obj);
}

QNetworkReply* KanboardInterface::moveColumnUp(int project_id, int column_id)
{
    QJsonObject obj;
    obj.insert("project_id", project_id);
    obj.insert("column_id", column_id);
    return createRequest("moveColumnUp", obj);
}

QNetworkReply* KanboardInterface::moveColumnDown(int project_id, int column_id)
{
    QJsonObject obj;
    obj.insert("project_id", project_id);
    obj.insert("column_id", column_id);
    return createRequest("moveColumnDown", obj);
}

QNetworkReply* KanboardInterface::updateColumn(int column_id, const QString& title, int task_limit)
{
    QJsonObject obj;
    obj.insert("column_id", column_id);
    obj.insert("title", title);
    obj.insert("task_limit", task_limit);
    return createRequest("updateColumn", obj);
}

QNetworkReply* KanboardInterface::addColumn(int project_id, const QString& title, int task_limit)
{
    QJsonObject obj;
    obj.insert("project_id", project_id);
    obj.insert("title", title);
    obj.insert("task_limit", task_limit);
    return createRequest("addColumn", obj);
}

QNetworkReply* KanboardInterface::removeColumn(int column_id)
{
    QJsonObject obj;
    obj.insert("column_id", column_id);
    return createRequest("removeColumn", obj);
}

QNetworkReply* KanboardInterface::getAllowedUsers(int project_id)
{
    QJsonObject obj;
    obj.insert("project_id", project_id);
    return createRequest("getAllowedUsers", obj);
}

QNetworkReply* KanboardInterface::revokeUser(int project_id, int user_id)
{
    QJsonObject obj;
    obj.insert("project_id", project_id);
    obj.insert("user_id", user_id);
    return createRequest("revokeUser", obj);
}

QNetworkReply* KanboardInterface::allowUser(int project_id, int user_id)
{
    QJsonObject obj;
    obj.insert("project_id", project_id);
    obj.insert("user_id", user_id);
    return createRequest("allowUser", obj);
}

QNetworkReply* KanboardInterface::createTask(const QString& title, int project_id, const QString& color_id,
                            int column_id, const QString& description, int owner_id,
                            int creator_id, int score, const QDateTime& dueDate, int category_id)
{
    QJsonObject obj;
    obj.insert("title", title);
    obj.insert("project_id", project_id);
    obj.insert("color_id", color_id);
    obj.insert("column_id", column_id);
    obj.insert("description", description);
    obj.insert("owner_id", owner_id);
    obj.insert("creator_id", creator_id);
    obj.insert("score", score);
    obj.insert("dueDate", dueDate.toString(Qt::ISODate));
    obj.insert("category_id", category_id);
    return createRequest("createTask", obj);
}

QNetworkReply* KanboardInterface::getTask(int task_id)
{
    QJsonObject obj;
    obj.insert("task_id", task_id);
    return createRequest("getTask", obj);
}

QNetworkReply* KanboardInterface::getAllTasks(int project_id)
{
    QJsonObject obj;
    obj.insert("project_id", project_id);
    return createRequest("getAllTasks", obj);
}

QNetworkReply* KanboardInterface::updateTask(int task_id,
                            const QString& title, int project_id, const QString& color_id,
                            int column_id, const QString& description, int owner_id,
                            int creator_id, int score, const QDateTime& dueDate, int category_id)
{
    QJsonObject obj;
    obj.insert("title", title);
    obj.insert("project_id", project_id);
    obj.insert("color_id", color_id);
    obj.insert("column_id", column_id);
    obj.insert("description", description);
    obj.insert("owner_id", owner_id);
    obj.insert("creator_id", creator_id);
    obj.insert("score", score);
    obj.insert("dueDate", dueDate.toString(Qt::ISODate));
    obj.insert("category_id", category_id);
    return updateTask(task_id, obj);
}

QNetworkReply* KanboardInterface::updateTask(int id, const QJsonObject& optionalValues)
{
    QJsonObject obj = optionalValues;
    obj.insert("id", id);
    return createRequest("updateTask", obj);
}

QNetworkReply* KanboardInterface::openTask(int task_id)
{
    QJsonObject obj;
    obj.insert("task_id", task_id);
    return createRequest("openTask", obj);
}

QNetworkReply* KanboardInterface::closeTask(int task_id)
{
    QJsonObject obj;
    obj.insert("task_id", task_id);
    return createRequest("closeTask", obj);
}

QNetworkReply* KanboardInterface::removeTask(int task_id)
{
    QJsonObject obj;
    obj.insert("task_id", task_id);
    return createRequest("removeTask", obj);
}

QNetworkReply* KanboardInterface::moveTaskPosition(int project_id, int task_id, int column_id, int position)
{
    QJsonObject obj;
    obj.insert("project_id", project_id);
    obj.insert("task_id", task_id);
    obj.insert("column_id", column_id);
    obj.insert("position", position);
    return createRequest("moveTaskPosition", obj);
}

QNetworkReply* KanboardInterface::createUser(const QString& username, const QString& password, const QString& confirmation, const QString& name, const QString& email, int is_admin, int default_project_id)
{
    QJsonObject obj;
    obj.insert("username", username);
    obj.insert("password", password);
    obj.insert("confirmation", confirmation);
    obj.insert("name", name);
    obj.insert("email", email);
    obj.insert("is_admin", is_admin);
    obj.insert("default_project_id", default_project_id);
    return createRequest("createUser", obj);
}

QNetworkReply* KanboardInterface::getUser(int user_id)
{
    QJsonObject obj;
    obj.insert("user_id", user_id);
    return createRequest("getUser", obj);
}

QNetworkReply* KanboardInterface::getAllUsers()
{
    QJsonObject obj;
    return createRequest("getAllUsers", obj);
}

QNetworkReply* KanboardInterface::updateUser(int user_id, const QString& username, const QString& password, const QString& confirmation, const QString& name, const QString& email, int is_admin, int default_project_id)
{
    QJsonObject obj;
    obj.insert("user_id", user_id);
    obj.insert("username", username);
    obj.insert("password", password);
    obj.insert("confirmation", confirmation);
    obj.insert("name", name);
    obj.insert("email", email);
    obj.insert("is_admin", is_admin);
    obj.insert("default_project_id", default_project_id);
    return createRequest("createUser", obj);
}

QNetworkReply* KanboardInterface::removeUser(int user_id)
{
    QJsonObject obj;
    obj.insert("user_id", user_id);
    return createRequest("removeUser", obj);
}

QNetworkReply* KanboardInterface::createCategory(int project_id, const QString& name)
{
    QJsonObject obj;
    obj.insert("project_id", project_id);
    obj.insert("name", name);
    return createRequest("createCategory", obj);
}

QNetworkReply* KanboardInterface::getCategory(int category_id)
{
    QJsonObject obj;
    obj.insert("category_id", category_id);
    return createRequest("getCategory", obj);
}

QNetworkReply* KanboardInterface::getAllCategories(int project_id)
{
    QJsonObject obj;
    obj.insert("project_id", project_id);
    return createRequest("getAllCategories", obj);
}

QNetworkReply* KanboardInterface::updateCategory(int category_id, const QString& name)
{
    QJsonObject obj;
    obj.insert("category_id", category_id);
    obj.insert("name", name);
    return createRequest("updateCategory", obj);
}

QNetworkReply* KanboardInterface::removeCategory(int category_id)
{
    QJsonObject obj;
    obj.insert("category_id", category_id);
    return createRequest("removeCategory", obj);
}

QNetworkReply* KanboardInterface::createComment(int task_id, int user_id, const QString& comment)
{
    QJsonObject obj;
    obj.insert("task_id", task_id);
    obj.insert("user_id", user_id);
    obj.insert("comment", comment);
    return createRequest("createComment", obj);
}
QNetworkReply* KanboardInterface::getComment(int comment_id)
{
    QJsonObject obj;
    obj.insert("comment_id", comment_id);
    return createRequest("getComment", obj);
}

QNetworkReply* KanboardInterface::getAllComments(int task_id)
{
    QJsonObject obj;
    obj.insert("task_id", task_id);
    return createRequest("getAllComments", obj);
}

QNetworkReply* KanboardInterface::updateComment(int comment_id, int task_id, int user_id, const QString& comment)
{
    QJsonObject obj;
    obj.insert("comment_id", comment_id);
    obj.insert("task_id", task_id);
    obj.insert("user_id", user_id);
    obj.insert("comment", comment);
    return createRequest("updateComment", obj);
}

QNetworkReply* KanboardInterface::removeComment(int comment_id)
{
    QJsonObject obj;
    obj.insert("comment_id", comment_id);
    return createRequest("removeComment", obj);
}

QNetworkReply* KanboardInterface::createSubtask(int task_id, const QString& title, int assignee_id, const QDateTime& timeEstimated, const QDateTime& timeSpent, int status)
{
    QJsonObject obj;
    obj.insert("task_id", task_id);
    obj.insert("title", title);
    obj.insert("assignee_id", assignee_id);
    obj.insert("timeEstimated", timeEstimated.toString(Qt::ISODate));
    obj.insert("timeSpent", timeSpent.toString(Qt::ISODate));
    obj.insert("status", status);
    return createRequest("createSubtask", obj);
}

QNetworkReply* KanboardInterface::getSubtask(int subtask_id)
{
    QJsonObject obj;
    obj.insert("subtask_id", subtask_id);
    return createRequest("getSubtask", obj);
}

QNetworkReply* KanboardInterface::getAllSubtasks(int task_id)
{
    QJsonObject obj;
    obj.insert("task_id", task_id);
    return createRequest("getAllSubtasks", obj);
}

QNetworkReply* KanboardInterface::updateSubtask(int subtask_id, int task_id, const QString& title, int assignee_id, const QDateTime& timeEstimated, const QDateTime& timeSpent, int status)
{
    QJsonObject obj;
    obj.insert("subtask_id", subtask_id);
    obj.insert("task_id", task_id);
    obj.insert("title", title);
    obj.insert("assignee_id", assignee_id);
    obj.insert("timeEstimated", timeEstimated.toString(Qt::ISODate));
    obj.insert("timeSpent", timeSpent.toString(Qt::ISODate));
    obj.insert("status", status);
    return createRequest("updateSubtask", obj);
}

QNetworkReply* KanboardInterface::removeSubtask(int subtask_id)
{
    QJsonObject obj;
    obj.insert("subtask_id", subtask_id);
    return createRequest("removeSubtask", obj);
}
