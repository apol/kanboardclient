/*
 *   Copyright 2014 Aleix Pol Gonzalez <aleixpol@kde.org>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU Library General Public License as
 *   published by the Free Software Foundation; either version 2, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details
 *
 *   You should have received a copy of the GNU Library General Public
 *   License along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#ifndef KANBOARDINTERFACE_H
#define KANBOARDINTERFACE_H

#include <QtCore/QObject>
#include <QUrl>

class QNetworkRequest;
class QNetworkReply;
class QNetworkAccessManager;

/**
 * See http://kanboard.net/documentation/api-json-rpc
 */
class KanboardInterface : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QUrl url READ url WRITE setUrl NOTIFY urlChanged)
    Q_PROPERTY(bool isAuthenticated READ isAuthenticated NOTIFY authenticatedChanged)
    public:
        KanboardInterface();

        static KanboardInterface* self();

        void setUrl(const QUrl& url);
        QUrl url() const;

        bool isAuthenticated() const;

        static QJsonValue fetchResult(QNetworkReply* reply);

        QNetworkReply* createProject(const QString& name);
        QNetworkReply* getProjectById(int project_id);
        QNetworkReply* getProjectByName(const QString& name);
        QNetworkReply* getAllProjects();
        QNetworkReply* updateProject(int project_id, const QString& name, bool is_active, const QString& token, bool is_public); //         TODO: make is_active, token and is_public optional arguments
        QNetworkReply* removeProject(int project_id);
        QNetworkReply* enableProject(int project_id);
        QNetworkReply* disableProject(int project_id);
        QNetworkReply* enableProjectPublicAccess(int project_id);
        QNetworkReply* disableProjectPublicAccess(int project_id);

        QNetworkReply* getBoard(int project_id);
        QNetworkReply* getColumns(int project_id);
        QNetworkReply* moveColumnUp(int project_id, int column_id);
        QNetworkReply* moveColumnDown(int project_id, int column_id);
        QNetworkReply* updateColumn(int column_id, const QString& title, int task_limit);
        QNetworkReply* addColumn(int project_id, const QString& title, int task_limit);
        QNetworkReply* removeColumn(int column_id);

        QNetworkReply* getAllowedUsers(int project_id);
        QNetworkReply* revokeUser(int project_id, int user_id);
        QNetworkReply* allowUser(int project_id, int user_id);
        QNetworkReply* createTask(const QString& title, int project_id, const QString& color_id,
                                  int column_id, const QString& description, int owner_id,
                                  int creator_id, int score, const QDateTime& dueDate, int category_id); //TODO: make optional arguments after project_id
        QNetworkReply* getTask(int task_id);
        QNetworkReply* getAllTasks(int project_id);
        QNetworkReply* updateTask(int id, const QJsonObject& optionalValues);
        Q_INVOKABLE QNetworkReply* updateTask(int id,
                                  const QString& title, int project_id, const QString& color_id,
                                  int column_id, const QString& description, int owner_id,
                                  int creator_id, int score, const QDateTime& dueDate, int category_id); //TODO: make optional arguments after task_id
        QNetworkReply* openTask(int task_id);
        QNetworkReply* closeTask(int task_id);
        QNetworkReply* removeTask(int task_id);
        QNetworkReply* moveTaskPosition(int project_id, int task_id, int column_id, int position);

        QNetworkReply* createUser(const QString& username, const QString& password, const QString& confirmation, const QString& name, const QString& email, int is_admin, int default_project_id); //TODO: make optional arguments after confirmation (yes, we send the confirmation too)
        QNetworkReply* getUser(int user_id);
        QNetworkReply* getAllUsers();
        QNetworkReply* updateUser(int user_id, const QString& username, const QString& password, const QString& confirmation, const QString& name, const QString& email, int is_admin, int default_project_id); //TODO: make optional arguments after confirmation
        QNetworkReply* removeUser(int user_id);

        QNetworkReply* createCategory(int project_id, const QString& name);
        QNetworkReply* getCategory(int category_id);
        QNetworkReply* getAllCategories(int project_id);
        QNetworkReply* updateCategory(int category_id, const QString& name);
        QNetworkReply* removeCategory(int category_id);

        QNetworkReply* createComment(int task_id, int user_id, const QString& comment);
        QNetworkReply* getComment(int comment_id);
        QNetworkReply* getAllComments(int task_id);
        QNetworkReply* updateComment(int comment_id, int task_id, int user_id, const QString& comment);
        QNetworkReply* removeComment(int comment_id);

        QNetworkReply* createSubtask(int task_id, const QString& title, int assignee_id, const QDateTime& timeEstimated, const QDateTime& timeSpent, int status); //TODO: make optional after title
        QNetworkReply* getSubtask(int subtask_id);
        QNetworkReply* getAllSubtasks(int task_id);
        QNetworkReply* updateSubtask(int subtask_id, int task_id, const QString& title, int assignee_id, const QDateTime& timeEstimated, const QDateTime& timeSpent, int status); //TODO: make optional after task_id
        QNetworkReply* removeSubtask(int subtask_id);

    signals:
        void urlChanged(const QUrl& url);
        void authenticatedChanged(bool isAuthenticated);

    private:
        QNetworkReply* createRequest(const QString& method, const QJsonObject& args);

        QNetworkAccessManager* m_manager;
        QUrl m_url;
        int m_idCount;
};

#endif // KANBOARDINTERFACE_H
