/*
 *   Copyright 2014 Aleix Pol Gonzalez <aleixpol@kde.org>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU Library General Public License as
 *   published by the Free Software Foundation; either version 2, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details
 *
 *   You should have received a copy of the GNU Library General Public
 *   License along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#include "kanboardqmlplugin.h"

#include <qqml.h>
#include <QNetworkReply>

#include "boardsmodel.h"
#include "tasksmodel.h"
#include "kanboardinterface.h"
#include "task.h"
#include "commentsmodel.h"
#include "subtasksmodel.h"

void KanboardQmlPlugin::registerTypes(const char *uri)
{
    Q_ASSERT(uri == QLatin1String("Kanboard"));

    auto interfaceAccess = [](QQmlEngine *, QJSEngine *) -> QObject* { return KanboardInterface::self(); };
    qmlRegisterSingletonType<KanboardInterface>(uri, 1, 0, "KanboardInterface", interfaceAccess );
    qmlRegisterType<BoardsModel>(uri, 1, 0, "BoardsModel");
    qmlRegisterType<TasksModel>(uri, 1, 0, "TasksModel");
    qmlRegisterType<Task>(uri, 1, 0, "Task");
    qmlRegisterType<SubtasksModel>(uri, 1, 0, "SubtasksModel");
    qmlRegisterType<CommentsModel>(uri, 1, 0, "CommentsModel");
    
    //needed as it's used in return types from kanboard interface
    qmlRegisterUncreatableType<QNetworkReply>(uri, 1, 0, "QNetworkReply", "Just no.");
}

#include "kanboardqmlplugin.moc"

