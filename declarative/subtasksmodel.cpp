/*
 *   Copyright 2014 Aleix Pol Gonzalez <aleixpol@kde.org>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU Library General Public License as
 *   published by the Free Software Foundation; either version 2, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details
 *
 *   You should have received a copy of the GNU Library General Public
 *   License along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#include "subtasksmodel.h"
#include "kanboardinterface.h"
#include <QtNetwork/QNetworkReply>
#include <QtCore/QJsonValue>
#include <QtCore/QJsonObject>
#include <QtCore/QDebug>

enum Roles {
    TitleRole = Qt::UserRole,
    StatusRole,
    AssigneeRole
};

SubtasksModel::SubtasksModel(QObject* parent)
    : QAbstractListModel(parent)
    , m_taskId(-1)
{
    connect(KanboardInterface::self(), SIGNAL(authenticatedChanged(bool)), SLOT(fetch()));
}

QHash< int, QByteArray > SubtasksModel::roleNames() const
{
    QHash<int, QByteArray> roles = QAbstractItemModel::roleNames();
    roles.insert(TitleRole, "title");
    roles.insert(StatusRole, "status");
    roles.insert(AssigneeRole, "assignee");

    return roles;
}

void SubtasksModel::setTaskId(int _taskId)
{
    qDebug() << "DAVE: fetching subtasks for " << _taskId;
    m_taskId = _taskId;
    fetch();
}

int SubtasksModel::taskId() const
{
    return m_taskId;
}

void SubtasksModel::fetch()
{
    if (!KanboardInterface::self()->isAuthenticated()) {
        qWarning() << "cannot fetch if not authenticated";
        return;
    }

    if (m_taskId < 0)
        return;

    QNetworkReply* reply = KanboardInterface::self()->getAllSubtasks(m_taskId);
    connect(reply, SIGNAL(finished()), SLOT(receivedAllSubtasks()));
}

void SubtasksModel::receivedAllSubtasks()
{
    QNetworkReply* reply = qobject_cast<QNetworkReply*>(sender());
    QJsonValue resultObject = KanboardInterface::fetchResult(reply);
    if (resultObject.isUndefined() || !resultObject.isArray()) {
        qCritical() << "wrong result" << resultObject;
        return;
    }

    beginResetModel();
    m_results = resultObject.toArray();
    endResetModel();
}

QVariant SubtasksModel::data(const QModelIndex& index, int role) const
{
    if (!index.isValid() || index.row()>=m_results.count())
        return QVariant();

    switch (role) {
        case TitleRole:
            return m_results[index.row()].toObject().value("title").toString();
        case StatusRole:
            return m_results[index.row()].toObject().value("status").toString();
        case AssigneeRole:
            return m_results[index.row()].toObject().value("assignee").toString();
    }
    return QVariant();
}

int SubtasksModel::rowCount(const QModelIndex& parent) const
{
    return parent.isValid() ? 0 : m_results.count();
}
