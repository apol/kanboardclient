/*
 *   Copyright 2014 Aleix Pol Gonzalez <aleixpol@kde.org>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU Library General Public License as
 *   published by the Free Software Foundation; either version 2, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details
 *
 *   You should have received a copy of the GNU Library General Public
 *   License along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#ifndef SUBTASKSMODEL_H
#define SUBTASKSMODEL_H

#include <QtCore/QAbstractItemModel>
#include <QtCore/QJsonArray>

class SubtasksModel : public QAbstractListModel
{
Q_OBJECT
Q_PROPERTY(int taskId READ taskId WRITE setTaskId)
public:
    enum Status {
        StateTodo,
        StateInProgress,
        StateDone,
    };
    Q_ENUMS(Status)

    SubtasksModel(QObject* parent = 0);

    virtual QHash<int, QByteArray> roleNames() const;
    virtual QVariant data(const QModelIndex& index, int role) const;
    virtual int rowCount(const QModelIndex& parent) const;

    int taskId() const;
    void setTaskId(int taskId);

private Q_SLOTS:
    void receivedAllSubtasks();
    void fetch();

private:

    int m_taskId;
    QJsonArray m_results;
};


#endif // COMMENTSMODEL_H
