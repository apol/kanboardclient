/*
 *   Copyright 2014 Aleix Pol Gonzalez <aleixpol@kde.org>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU Library General Public License as
 *   published by the Free Software Foundation; either version 2, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details
 *
 *   You should have received a copy of the GNU Library General Public
 *   License along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#include "task.h"
#include "kanboardinterface.h"
#include <QtNetwork/QNetworkReply>
#include <QtCore/QJsonValue>
#include <QtCore/QJsonObject>

Task::Task(QObject* task)
    : QObject(task)
    , m_task_id(-1)
{
}

void Task::fetch()
{
    Q_ASSERT(m_task_id>=0);
    QNetworkReply* reply = KanboardInterface::self()->getTask(m_task_id);
    connect(reply, SIGNAL(finished()), this, SLOT(taskFetched()));
}

void Task::taskFetched()
{
    QNetworkReply* reply = qobject_cast<QNetworkReply*>(sender());
    QJsonValue resultObject = KanboardInterface::fetchResult(reply);
    Q_ASSERT(resultObject.isObject());
    QJsonObject obj = resultObject.toObject();

    m_title = obj.value("title").toString(tr("Unnamed"));
    m_project_id = obj.value("project_id").toInt();
    m_color_id = obj.value("color_id").toString("yellow");
    m_column_id = obj.value("m_column_id").toInt(0);
    m_description = obj.value("description").toString();
    m_owner_id = obj.value("owner_id").toInt();
    m_creator_id = obj.value("creator_id").toInt();
    m_score = obj.value("score").toInt();
    m_dueDate = QDateTime::fromString(obj.value("dueDate").toString(), Qt::ISODate);
    m_category_id = obj.value("category_id").toInt();

    emit taskChanged();
}

void Task::setTaskId(int taskId)
{
    m_task_id = taskId;
    emit taskIdChanged();

    fetch();
}

int Task::taskId() const
{
    return m_task_id;
}

void Task::moveTaskPosition(int column_id, int position)
{
    QNetworkReply* reply = KanboardInterface::self()->moveTaskPosition(m_project_id, m_task_id, column_id, position);
    connect(reply, SIGNAL(finished()), SLOT(fetch()));
}

template<typename T>
void Task::updateValue(const QString& key, T newValue)
{
    Q_ASSERT(m_task_id>=0);
    QJsonObject obj;
    obj.insert(key, newValue);
    QNetworkReply* reply = KanboardInterface::self()->updateTask(m_task_id, obj);
    connect(reply, SIGNAL(finished()), SLOT(fetch()));
}

void Task::setTitle(const QString& newTitle)
{
    updateValue("title", newTitle);
}

void Task::setColorId(const QString& newColorId)
{
    updateValue("color_id", newColorId);
}

void Task::setDescription(const QString& newDescription)
{
    updateValue("description", newDescription);
}

void Task::setDueDate(const QDateTime& newDueDate)
{
    updateValue("dueDate", newDueDate.toString(Qt::ISODate));
}

void Task::setScore(int newScore)
{
    updateValue("score", newScore);
}

QString Task::title() const
{
    return m_title;
}

QString Task::colorId() const
{
    return m_color_id;
}

QString Task::description() const
{
    return m_description;
}

QDateTime Task::dueDate() const
{
    return m_dueDate;
}

int Task::score() const
{
    return m_score;
}

#include "task.moc"
