/*
 *   Copyright 2014 Aleix Pol Gonzalez <aleixpol@kde.org>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU Library General Public License as
 *   published by the Free Software Foundation; either version 2, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details
 *
 *   You should have received a copy of the GNU Library General Public
 *   License along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#ifndef TASK_H
#define TASK_H

#include <QtCore/QObject>
#include <QtCore/QDateTime>

class Task : public QObject
{
    Q_OBJECT
    Q_PROPERTY(int taskId READ taskId WRITE setTaskId NOTIFY taskIdChanged)
    Q_PROPERTY(QString title READ title WRITE setTitle NOTIFY taskChanged)
    Q_PROPERTY(QString colorId READ colorId WRITE setColorId NOTIFY taskChanged)
    Q_PROPERTY(QString description READ description WRITE setDescription NOTIFY taskChanged)
    Q_PROPERTY(int score READ score WRITE setScore NOTIFY taskChanged)
    Q_PROPERTY(QDateTime dueDate READ dueDate WRITE setDueDate NOTIFY taskChanged)
    public:
        Task(QObject* task = 0);

        int taskId() const;
        void setTaskId(int taskId);

        QString title() const;
        void setTitle(const QString& newTitle);
        QString colorId() const;
        void setColorId(const QString& newColorId);
        QString description() const;
        void setDescription(const QString& newDescription);
        int score() const;
        void setScore(int newScore);
        QDateTime dueDate() const;
        void setDueDate(const QDateTime& newDueDate);

    public slots:;
        void moveTaskPosition(int column_id, int position);

    signals:
        void taskChanged();
        void taskIdChanged();

    private slots:
        void taskFetched();
        void fetch();

    private:
        template<typename T>
        void updateValue(const QString& key, T value);

        int m_task_id;
        QString m_title;
        int m_project_id;
        QString m_color_id;
        int m_column_id;
        QString m_description;
        int m_owner_id;
        int m_creator_id;
        int m_score;
        QDateTime m_dueDate;
        int m_category_id;
};

#endif // TASK_H
