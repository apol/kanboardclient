/*
 *   Copyright 2014 Aleix Pol Gonzalez <aleixpol@kde.org>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU Library General Public License as
 *   published by the Free Software Foundation; either version 2, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details
 *
 *   You should have received a copy of the GNU Library General Public
 *   License along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#include "tasksmodel.h"
#include "kanboardinterface.h"
#include <QtNetwork/QNetworkReply>
#include <QtCore/QJsonDocument>
#include <QtCore/QJsonObject>
#include <QtCore/QDebug>

TasksModel::TasksModel(QObject* parent)
    : QAbstractItemModel(parent)
    , m_projectId(-1)
    , m_fetching(0)
{
}

void TasksModel::fetch()
{
    Q_ASSERT(m_projectId>=0);
    QNetworkReply* reply = KanboardInterface::self()->getBoard(m_projectId);
    connect(reply, SIGNAL(finished()), SLOT(receivedBoard()));
    m_fetching++;
    emit fetchingChanged();
}

void TasksModel::receivedBoard()
{
    m_fetching--;
    QNetworkReply* reply = qobject_cast<QNetworkReply*>(sender());
    QJsonValue resultObject = KanboardInterface::fetchResult(reply);
    if (resultObject.isUndefined() || !resultObject.isArray()) {
        qCritical() << "wrong result" << resultObject;
        emit fetchingChanged();
        return;
    }

    beginResetModel();
    m_columns = resultObject.toArray();
    endResetModel();

    emit fetchingChanged();
}

int TasksModel::rowCount(const QModelIndex& parent) const
{
    if (parent.isValid() && !parent.parent().isValid()) {
        return m_columns[parent.row()].toObject().value("tasks").toArray().count();
    } else if (!parent.isValid()) {
        return m_columns.count();
    }
    return 0;
}

QModelIndex TasksModel::index(int row, int column, const QModelIndex& parent) const
{
    return createIndex(row, column, parent.isValid() ? parent.row()+1 : 0);
}

QModelIndex TasksModel::parent(const QModelIndex& child) const
{
    return child.internalId()==0 ? QModelIndex() : createIndex(child.internalId()-1, 0, quintptr(0));
}

QVariant TasksModel::data(const QModelIndex& index, int role) const
{
    if (!index.isValid())
        return QVariant();

    int row = index.row();
    if(index.internalId()==0) {
        if (row>=m_columns.count())
            return QVariant();

        switch(role) {
            case Qt::DisplayRole:
                return m_columns[row].toObject().value("title").toString();
            case Qt::UserRole:
                return m_columns[row];
        }
    } else {
        QModelIndex p = index.parent();
        QJsonArray obj = m_columns[p.row()].toObject().value("tasks").toArray();
        if (row>=obj.count() || !p.isValid())
            return QVariant();

        switch(role) {
            case Qt::DisplayRole:
                return obj[row].toObject().value("title").toString();
            case Qt::UserRole:
                return obj[row].toObject();
        }
    }
    return QVariant();
}

void TasksModel::setProjectId(int id)
{
    m_projectId = id;
    emit projectIdChanged();

    fetch();
}

int TasksModel::projectId() const
{
    return m_projectId;
}

QHash<int, QByteArray> TasksModel::roleNames() const
{
    QHash<int, QByteArray> ret = QAbstractItemModel::roleNames();
    ret.insert(Qt::UserRole, "info");
    return ret;
}

bool TasksModel::isFetching() const
{
    return m_fetching > 0;
}

void TasksModel::moveTaskPosition(int task_id, int column_id, int position)
{
    auto *ret = KanboardInterface::self()->moveTaskPosition(m_projectId, task_id, column_id, 1);
    connect(ret, &QNetworkReply::finished, [=](){qDebug() << ret->readAll();});

}



#include "tasksmodel.moc"
