/*
 *   Copyright 2014 Aleix Pol Gonzalez <aleixpol@kde.org>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU Library General Public License as
 *   published by the Free Software Foundation; either version 2, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details
 *
 *   You should have received a copy of the GNU Library General Public
 *   License along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#ifndef TASKSMODEL_H
#define TASKSMODEL_H

#include <QtCore/QAbstractItemModel>
#include <QtCore/QJsonArray>

class TasksModel : public QAbstractItemModel
{
    Q_OBJECT
    Q_PROPERTY(int projectId READ projectId WRITE setProjectId NOTIFY projectIdChanged)
    Q_PROPERTY(bool isFetching READ isFetching NOTIFY fetchingChanged)
    public:
        explicit TasksModel(QObject* parent = 0);
        void fetch();

        int projectId() const;
        void setProjectId(int id);

        bool isFetching() const;

        virtual int columnCount(const QModelIndex& ) const { return 1; }
        virtual int rowCount(const QModelIndex& parent = QModelIndex()) const;
        virtual QModelIndex parent(const QModelIndex& child) const;
        Q_INVOKABLE virtual QModelIndex index(int row, int column, const QModelIndex& parent = QModelIndex()) const;
        virtual QVariant data(const QModelIndex& index, int role = Qt::DisplayRole) const;
        virtual QHash<int, QByteArray> roleNames() const;

    signals:
        void projectIdChanged();
        void fetchingChanged();
        
    public slots:
        void moveTaskPosition(int task_id, int column_id, int position);


    private slots:
        void receivedBoard();

    private:
        int m_projectId;
        QJsonArray m_columns;
        int m_fetching;
};

#endif // TASKSMODEL_H
