/*
 *   Copyright 2014 Aleix Pol Gonzalez <aleixpol@kde.org>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU Library General Public License as
 *   published by the Free Software Foundation; either version 2, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details
 *
 *   You should have received a copy of the GNU Library General Public
 *   License along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

import QtQuick 2.0
import QtQuick.Controls 1.0
import QtQuick.Layouts 1.0

import Kanboard 1.0
import org.kde.kquickcontrolsaddons 2.0 as KQuick

ApplicationWindow
{
    ColumnLayout {
        anchors.fill: parent

        RowLayout {
            Layout.fillWidth: true
            TextField {
                Layout.fillWidth: true
                id: urlField
                placeholderText: "The url, with auth"
                text: "http://jsonrpc:5c0c3cb2d96714a115441c572c978c7c12340fa59960a9d054c2f7971d376985@localhost:8080/jsonrpc.php"
            }
            Button {
                text: "Go"
                onClicked: KanboardInterface.url = urlField.text
            }
        }
        ComboBox {
            Layout.fillWidth: true
            model: BoardsModel {}
            textRole: "display"
            onCurrentTextChanged: {
                tasksModel.projectId = currentIndex+1
            }
        }
        TableView {
            id: columnsView
            height: 200
            Layout.fillWidth: true
            model: TasksModel { id: tasksModel }
            TableViewColumn{ role: "info"  ; title: "Column Name" ; width: 100 }

            itemDelegate: Item {
                Text {
                    anchors.verticalCenter: parent.verticalCenter
                    color: styleData.textColor
                    elide: styleData.elideMode
                    text: styleData.value.title
                }
            }
        }

        RowLayout {
            Layout.fillHeight: true

            ListView {
                Layout.fillHeight: true
                Layout.fillWidth: true
                delegate: RowLayout {
                    Label {
                        text: display
                    }
                    Button {
                        text: "show"
                        onClicked: {
                            subtasksModel.taskId = model.info.id
                            commentsModel.taskId = model.info.id
                        }
                    }
                }

                model: KQuick.ColumnProxyModel {
                    rootIndex: tasksModel.index(columnsView.currentRow, 0)
                }
            }
            TableView {
                Layout.fillHeight: true
                TableViewColumn{ role: "display"  ; title: "Comments" ; width: 100 }

                model: CommentsModel {
                    id: commentsModel
                }
            }
            TableView {
                Layout.fillHeight: true
                TableViewColumn{ role: "display"  ; title: "Subtasks" ; width: 100 }

                model: SubtasksModel {
                    id: subtasksModel
                }
            }
        }
    }
}

